from contextlib import asynccontextmanager

from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()


class MiddlewareDB:
    def __init__(self, db_url):
        self.db_url = db_url
        self.engine = create_async_engine(self.db_url, echo=False, pool_size=50, pool_pre_ping=True)
        self.session = sessionmaker(self.engine, expire_on_commit=False, class_=AsyncSession)

    async def setup_db(self):
        async with self.engine.begin() as conn:
            await conn.run_sync(Base.metadata.drop_all)
            await conn.run_sync(Base.metadata.create_all)

    @asynccontextmanager
    async def get_session(self):
        async with self.session() as session:
            async with session.begin():
                yield session


db_uri = "postgresql+asyncpg://myuser:mypassword@localhost:5432/mydatabase"
db = MiddlewareDB(db_uri)
