from sqlalchemy import select

from db import db, MiddlewareDB, db_uri
from user_model import User

a = db


async def wrong_stress_task(task_id):
    print(f"Starting test task: {task_id}")
    _db = MiddlewareDB(db_uri)
    async with _db.get_session() as session:
        print("session retrieved")
        test_user = await session.execute(select(User).filter_by(name="test user").limit(1))
        test_user.ops = test_user.ops + 1
        session.commit()
        print(f"User updated: {test_user.name}, {test_user.last_name}, {test_user.ops}")
        print(f"Task finished: {task_id}")
