from sqlalchemy import select

from db import db
from user_model import User


async def better_stress_task(task_id):
    print(f"Starting test task: {task_id}")
    async with db.get_session() as session:
        print("session retrieved")
        stmt = select(User).filter_by(name="test user").limit(1).with_for_update()
        result = await session.execute(stmt)
        test_user = result.scalars().one()
        test_user.ops = test_user.ops + 1
        await session.commit()
        print(f"User updated: {test_user.name}, {test_user.last_name}, {test_user.ops}")
        print(f"Task finished: {task_id}")
