import asyncio

from sqlalchemy import text

from better_module import better_stress_task
from db import db
from user_model import User


async def test_task(task_id):
    print(f"Starting test task: {task_id}")
    await asyncio.sleep(1)
    print(f"Task finished: {task_id}")


async def setup_test():
    async with db.get_session() as session:
        print("session retrieved")
        user = User(name="test user", last_name="test user last name", ops=0)
        session.add(user)
        await session.commit()


async def max_connections():
    async with db.engine.connect() as connection:
        result = await connection.execute(text("SHOW max_connections;"))
        return result.scalar()


async def main():
    await db.setup_db()
    await setup_test()
    max_cons = await max_connections()
    print(f"Database setup complete. Server can handle {max_cons} connections")
    tasks = []
    # for i in range(100):
    #     task = asyncio.ensure_future(wrong_stress_task(i))
    #     tasks.append(task)
    for i in range(1000):
        task = asyncio.ensure_future(better_stress_task(i))
        tasks.append(task)

    await asyncio.gather(*tasks)


if __name__ == '__main__':
    asyncio.run(main())
